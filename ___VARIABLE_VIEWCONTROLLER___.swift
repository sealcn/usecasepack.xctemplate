//___FILEHEADER___

import Foundation
import UIKit
import RxSwift
import RxCocoa
import SnapKit

class ___VARIABLE_VIEWCONTROLLER___: UIViewController {
    private let vm: ___VARIABLE_MODULENAME___ViewModel
    private let disposeBag = DisposeBag()

    init(vm: ___VARIABLE_MODULENAME___ViewModel) {
        self.vm = vm
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraits()
        doAfterProcessViews()
        bindModel()
    }

    func setupView() {

    }

    func setupConstraits() {

    }

    func doAfterProcessViews() {

    }

    func bindModel() {
        let input = ___VARIABLE_MODULENAME___ViewModel.Input()
        let output = vm.transform(input: input)
    }
}
