//___FILEHEADER___

import Foundation
import RxSwift
import RxCocoa

class ___VARIABLE_MODULENAME___ViewModel: ViewModelType {
    private let usecase: ___VARIABLE_MODULENAME___UseCaseProtocol
    private let navigator: ___VARIABLE_MODULENAME___Navigator

    private let disposeBag = DisposeBag()

    struct Input {
        fatalError("implete me")
    }

    struct Output {
        fatalError("implete me")
    }


    init(usecase: ___VARIABLE_MODULENAME___UseCaseProtocol, navigator: ___VARIABLE_MODULENAME___Navigator) {
        self.usecase = usecase
        self.navigator = navigator
    }

    func transform(input: Input) -> Output {

        return Output()
    }
}
